import Vue from 'vue';
import vueRouter from 'vue-router';
Vue.use(vueRouter);

//默认主体
import bodys from '../components/Bodys.vue';
//登陆界面
import signIn from '../components/SignIn.vue'
//个人信息界面
import userInfo from '../components/UserInfo.vue'

const routes = [
  {path:'',component:bodys},
  {path: '/signin',component: signIn},
  {path: '/userinfo',component: userInfo,beforeEnter:(to,from,next)=>{
    if(sessionStorage.getItem('name')){
      next();
    }else{
      alert('请先登录');
    }
    }},
  {path:'*',required:'/'}
]

export const router = new vueRouter({
  routes,
  mode:'history'
})
